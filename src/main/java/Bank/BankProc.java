package Bank;

import SimpleClasses.Account;
import SimpleClasses.Person;
import SimpleClasses.Raport;

public interface BankProc {
    /**
     * @pre a != null
     * @post (getSize () == getSize()@pre + 1)
     */
    public void addPerson(Person p);

    /**
     * @pre p != null
     * @post (getSize () == getSize()@pre - 1) || @nochange
     */
    public void removePerson(Person p);

    /**
     * @pre a != null && p != null
     * @post (getSize () == getSize()@pre + 1)
     */
    public void addAccount(Account a, Person p);

    /**
     * @pre a!= null && p != null
     * @post (getSize () == getSize()@pre - 1) || @nochange
     */
    public void removeAccount(Account a, Person p);

    /***
     * @pre a!=null && p != null
     * @post @nochange
     */
    public Account readAccountData(Account a, Person p);

    /***
     * @pre a!=null && p != null
     * @post isWellFormed
     */
    public void writeAccountData(Account a, Person p);

    /**
     * @pre p!=null
     * @post r != null
     * */
    public Raport generateRaport(Person p);

}
