package SimpleClasses;

import java.util.Observable;
import java.util.Observer;

public class Person implements Observer {
    private String name;
    private int age;

    public Person (){
        super();
        name = "John";
        age  = 20;
    }

    public Person (String name, int age){
        super();
        this.name = name;
        this.age = age;
    }

    public void update(Observable o, Object arg) {
        System.out.println("Pentru persoana " + name + " a fost facuta o schimbare in cont");
    }

    public int hashCode(){
        int cod = name.hashCode() + age;
        return cod;
    }

    public boolean equals(Object o){
        Person p = (Person) o;
        if(name.equals(p.getName())== true && age==p.getAge() )
            return true;
        return false;
    }

    public void setAge(int age){
        this.age = age;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getAge(){
        return age;
    }

    public String getName(){
        return name;
    }
}
