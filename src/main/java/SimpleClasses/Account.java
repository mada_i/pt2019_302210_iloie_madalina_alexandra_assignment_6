package SimpleClasses;

import java.util.Observable;

public class Account extends Observable {
    public Person holder;
    public int ammount;

    public Account(){
        super();
        holder = new Person();
        ammount = 0;
    }

    public Account (Person holder, int ammount){
        this.holder = holder;
        this.ammount = ammount;
    }

    public void addMoney(int money){
        if(money > 0)
            ammount += money;
        else
            System.out.println("Cannot add negative ammount");
    }

    public void withraw(int money){
        if(money > 0){
            if(ammount - money < 0)
                ammount -= money;
        }
    }

}
